.. _index:

Programmation orientée objet
============================

.. image:: img/intro_poo.jpeg
    :align: center
    :class: margin-bottom-8

Le paradigme de **programmation orientée objet** est une forme de programmation basée sur des objets que l'on crée. Chaque **objet** est créé à partir d'un modèle.

Le modèle qui sert à la création d'un objet définit:

-   des **attributs** qui sont des variables qui stockent des valeurs;
-   des **méthodes** qui sont des fonctions pour agir sur chaque objet et les valeurs de ses attributs.

.. figure:: img/objet.svg
    :align: center
    :width: 200

.. note::

    Certains langages comme JAVA sont des langages uniquement basé sur les objets. En Python, qui est un langage multi-paradigme, il est possible de réaliser de la programmation objet.

.. toctree::
    :maxdepth: 1
    :hidden: 

    content/cours.rst
    content/exercice_0.rst
    content/exercice_1.rst
    content/tp_geometrie.rst

