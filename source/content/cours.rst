Programmation objet en Python
===============================

En python,  un objet est défini par le mot clef ``class`` suivi du nom de l'objet et des 2 points.

-   Une **classe** est le modèle qui permet de créer des objets.   
-   Une **classe** définit les ``attributs`` d'un objet et les ``méthodes`` qui lui sont propres.

.. code-block:: python

    class Nom_classe:

        # on définit les attributs

        # on définit les méthodes

.. warning::

    -   Tout ce qui est défini dans la classe doit être **indenté**.
    -   Par convention, les noms de classe commencent par une majuscule.

Les attributs d'un objet
------------------------

Les **attributs** d'un objet permettent de stocker des valeurs pour notre objet. Pour créer les attributs de notre classe, on utilise un **constructeur** qui est une fonction appelée chaque fois que l'on crée un nouvel objet. Ce constructeur est la fonction ``__init__`` contenant le paramètre obligatoire ``self``.

.. code-block:: python

    class Objet:

        # Constructeur
        def __init__(self):
            self.attribut_1 = valeur_1
            self.attribut_2 = valeur_2
            ...
            self.attribut_n = valeur_n

.. note::

    -   On peut définir autant d'attributs qu'on le souhaite;
    -   Tous les objets créés ont les mêmes noms d'attributs ce qui rend la structure plus cohérente;
    -   Un objet définit une structure de données.

.. admonition:: Exemple

    Imaginons que l'on souhaite créer un objet en python représentant une voiture. Les attributs de notre objet voiture sont des caractéristiques comme le modèle de voiture, le nombre de portes et la couleur.
    
    #.  Écrire la classe ``Automobile`` qui définit un modèle de voiture.
    #.  Ajouter le constructeur qui définit les attributs de notre modèle.
    #.  Créer un objet ``v1`` représentant une clio rouge avec 5 portes.
    #.  Créer un objet ``v2`` représentant une c1 noire avec 3 portes.
    
.. pyscript::
    :title: Constructeur de classe
    :height: 100px

    # classe Automobile à créer !

.. hint::

   Les variables ``v_1`` et ``v_2`` sont 2 **instances d'objets** construites avec la classe ``Automobile``. On fait souvent le raccourci que ``v_1`` et ``v_2`` sont deux **objets** de la classe ``Automobile``.

Chaque nouvel objet créé a des attributs qui ont les mêmes valeurs initiales. Cela implique de modifier les valeurs des attributs après la création de l'objet !

Pour éviter cela, le constructeur étant une fonction, on ajoute des paramètres. Les paramètres sont alors associés aux attributs de l'objet:

.. code-block:: python

    class Ma_classe:

        # Constructeur
        def __init__(self,parametre_1,parametre_2,...,parametre_n):
            self.attribut_1 = parametre_1
            self.attribut_2 = parametre_2
            ...
            self.attribut_n = parametre_n

.. admonition:: Exemple

    On reprend notre classe ``Automobile`` précédente.
    
    #.  Ajouter au constructeur de l'exemple précédent trois paramètres ``m``, ``nb_portes`` et ``coul`` en les associants à chaque attribut.
    #.  Recréer les objets ``v1`` et ``v2`` et vérifiez la valeur de leurs attributs.

.. pyscript::

    # classe Automobile avec des paramètres.

Les méthodes d'un objet
-----------------------

Les **méthodes** sont des fonctions propres aux objets, ce qui implique que la fonction ne peut être appliquée qu'à l'objet. En python, une méthode d'objet est définie dans la classe. Pour faire référence à l'objet, nous devons utiliser le mot clé **self** qui désigne l'objet. Ce mot clé **self** est passé en paramètre.

.. code-block:: python

    class Objet:

        # Constructeur
        def __init__(self,parametre_1,parametre_2,...,parametre_n):
            self.attribut_1 = parametre_1
            self.attribut_2 = parametre_2
        
        # Méthode
        def ma_methode(self):
            # modifier un attribut
            self.attribut_1 = self.attribut_1 + 1
            
            # afficher un attribut
            print(self.attribut_2)

            # renvoie d'une valeur
            return self.attribut_1, self.attribut_2

.. admonition:: Exemple

    Reprenons l'exemple de la classe ``Automobile`` et créons une méthode pour savoir si la voiture roule. On modifie la classe en ajoutant l'attribut ``vitesse`` initialisé avec la valeur ``0``.

    #.  Reprendre la classe ``Automobile`` et ajouter l'attibut ``vitesse``.
    #.  La fonction ``roule`` renvoie un booléen qui vaut ``True`` si la voiture roule et ``False`` dans le cas contraire.
    #.  Recréer les 2 objets ``v_1`` et ``v2``. On considère que ``v1`` roule et que ``v2`` est à l'arrêt.
    #.  Appliquer la méthode ``roule`` à nos deux objets.

.. pyscript::

.. rubric:: Méthode avec paramètres

Une méthode est une fonction, donc on peut ajouter des paramètres à celle-ci comme le montre le code suivant:

.. code-block:: python

    class Objet:

        # constructeur
        def __init__(self):
            self.attribut_1 = valeur_1
            self.attribut_2 = valeur_2

        # Méthode
        def ma_methode_avec_parametres(self,p1,p2):
            
            self.attribut_1 = self.attribut_1 + p1
            if p2 <= 0:
                self.attribut_2 = 0
            else:
                self.attribut_2 = self.attribut_2 * p2

.. admonition:: Exemple

    Dans la classe ``Automobile``, on ajoute une méthode ``ralentir`` qui diminue la vitesse de la voiture d'une valeur donnée en paramètre. La vitesse ne peut pas être négative !

    #.  Ajouter la méthode ``ralentir`` à la classe ``Automobile``.
    #.  Appliquer la méthode aux objets ``v1`` et ``v2``.


