Exercices d'approfondissement
=============================

.. exercice::
	
	On définit l'objet ``Compte_bancaire`` dont voici l'interface :
	
	-   Attributs : ``solde`` et ``titulaire``
	-   Méthodes : ``est_positif``, ``crediter``, ``debiter``, ``virer_vers``, ``afficher`` ou ``__repr__``
	
	#.	Écrire la classe ``Compte_bancaire``, son constructeur ``__init__`` puis les différentes méthodes.
	#.	Tester en créant un compte bancaire **A** avec un solde de 1000 euros et un compte bancaire **B** avec un solde bancaire de 200 euros.
	#.	Effectuer un virement de 300 euros de A vers B.
	#.	Interdire tout virement si le solde du compte est négatif.

.. exercice::

	Le jeu de cartes peut être construit avec différentes structures de données Python. On se propose de créer un tel jeu avec deux classes. La classe ``Carte`` pour chaque carte du jeu et la classe ``Paquet`` qui sera composé de 52 objets de type Carte. 

	.. image:: ../img/objet_carte.svg
		:align: center
		:width: 360px
		
	#.	Écrire le constructeur de la classe ``Carte`` avec 2 paramètres ``c`` et ``v`` qui permettent d'initialiser les attributs ``couleur`` et ``valeur`` de la carte. 
	
		-	Les valeurs des cartes sont des nombres entiers de 1 à 13; 11 pour le valet, 12 pour la dame et 13 pour le roi.
		-	Les couleurs sont ``coeur``, ``carreau``, ``pique`` et ``trèfle``.

	#.	Écrire la méthode ``get_couleur`` qui renvoie la couleur de la carte, c'est à dire une valeur parmi ``coeur``, ``carreau``, ``pique`` et ``trèfle``. La valeur renvoyée est une chaine de caractères.
	#.	Écrire la méthode ``get_valeur`` qui renvoie la valeur de la carte. On distinguera les cas particuliers des cartes **as**, **valet**, **dame** et **roi**.
	#.	Écrire le constructeur de la classe ``Paquet`` qui initialise l'attibut ``contenu`` avec une liste vide.
	#.	La méthode ``remplir`` ajoute à la liste ``contenu`` les 52 cartes du jeu. Chaque carte étant un objet ``Carte``. Écrire cette méthode de la classe ``Paquet``.
	#.	La méthode ``melanger`` se charge de mélanger de façon aléatoire le contenu du paquet de cartes. Écrire la méthode ``melanger`` de la classe ``Paquet``. La méthode ne renvoie rien. On pourra afficher un message qui précise que le paquet est mélangé !
	#.	La méthode ``get_carte_at`` prend en paramètre un entier ``n`` entre 1 et 52 et renvoie la carte qui se trouve en position ``n`` dans le paquet. Écrire la méthode ``get_carte_at`` de la classe ``Paquet`` en faisant attention au décalage.
	#.	Écrire un programme principal qui :

		-   fabrique un paquet de 52 cartes
		-   mélange les cartes du paquet
		-	tire au hasard une carte en choisissant une position aléatoirement
		-	affiche la carte tirée au hasard.

.. exercice::

	.. image:: ../img/jeu_dominos.jpg
		:align: center
		:width: 480
   
	Le jeu de dominos est constitué de 28 pièces. Une pièce de domino est en deux parties. Chaque partie contient de 0 à 6 points. Lorsque les deux parties ont le même nombre de points, les dominos sont doubles. Il y a 7 doubles dans un jeu : du double 0 au double 6.
	
	#.	La classe ``Domino`` définit l'objet **domino** avec un seul attribut ``valeur``. L'attribut est un tuple représentant les points de chaque moitié du domino.
	
		.. image:: ../img/classe_Domino_2.svg
			:align: center
			:width: 200
			
		Les méthodes de la classe ``Domino`` sont:
		
		-   Le constructeur ``__init__`` qui initialise l'attribut ``valeur`` avec un tuple dont les valeurs sont passées en paramètres.
		-   La méthode ``somme_points`` qui renvoie la somme des points du domino.
		-   La méthode ``__repr__`` qui affiche le domino, par exemple ``[a|b]``.

		On donne ci-dessous le code de la classe à compléter:

		.. literalinclude:: ../python/les_dominos.py
			:lines: 3-28

	#.	La classe ``Jeu`` a un seul attribut et 2 méthodes.
	
		L'attribut ``dominos`` est une liste contenant les 28 dominos du jeu. Chaque domino est un objet ``Domino``.
	
		Les méthodes de la classe ``Jeu`` sont:

		- 	Le constructeur ``__init__`` qui initialise l'attribut ``dominos`` avec une liste dont les valeurs sont les 28 dominos du jeu.
		- 	La méthode mélange qui range dans un ordre aléatoire la liste des 28 dominos.
		
			.. image:: ../img/classe_JeuDomino_2.svg
				:align: center
				:width: 200

		On donne ci-dessous le code de la classe à compléter:

		.. literalinclude:: ../python/les_dominos.py
			:lines: 30-46

	#.	La classe ``Joueur`` a 3 attributs et 2 méthodes.

		-   L'attribut ``nom`` contient le nom du joueur
		- 	L'attribut ``age`` contient l'age du joueur
		- 	L'attribut ``dominos`` est une liste qui contient les dominos du joueur

		Les méthodes de cette classe sont:

		-   Le constructeur ``__init__`` initialise les attributs ``nom`` et ``age`` avec les valeurs passées en paramètres et l'attribut ``dominos`` avec une liste vide.
		-   La méthode ``piocher`` ajoute un domino au joueur en le retirant de l'objet construit avec la classe ``Jeu``.

		.. image:: ../img/classe_Joueur_2.svg
			:align: center
			:width: 200

		On donne ci-dessous le code de la classe à compléter:

		.. literalinclude:: ../python/les_dominos.py
			:lines: 50-67
			
	#.	Le programme principal:

		-   On crée un jeu de dominos
		-   On crée 3 joueurs
		-   Chaque joueur pioche chacun son tour un domino dans le jeu jusqu'à 7.
		-   On affiche les dominos de chaque joueur et les dominos restant dans la pioche.
		-   On détermine le joueur qui doit commencer la partie. 

		On donne les grandes lignes du programme principal ci-dessous:

		.. literalinclude:: ../python/les_dominos.py
			:lines: 70-87

		Compléter ce programme principal en remplaçant les ``pass`` par les lignes de code qui conviennent.

.. exercice::

	En Python, les nombres entiers et flottants sont des objets. Lorsqu'on compare 2 nombres entiers, Python fait appel aux méthodes ``__eq__``, ``__lt__`` ou ``__gt__``. De même lorsqu'on calcule une addition ou une multiplication, Python utilise les méthodes ``__add__`` et ``__mul__``.

	Les fractions sont des nombres dits rationnels de la forme :math:`\dfrac{n}{d}`. Le nombre :math:`n` est le **numérateur** et le nombre :math:`d` est le **dénominateur**. Ce sont des nombres entiers avec :math:`d` strictement positif.

	On va définir une classe ``Fraction`` avec les deux attributs : ``num`` et ``denom``. On ajoutera des méthodes pour comparer, additionner et multiplier deux fractions.

	#.	 Écrire le constructeur de cette classe. On prend en compte le dénominateur strictement positif.
	#.	 Ajouter une méthode ``__str__`` qui renvoie une chaine de caractères de la forme "n/d" ou simplement "n" lorsque le dénominateur est égal à 1.
	#.	Créer les objets ``a``, ``b`` et ``c`` associés aux fractions :math:`\dfrac{1}{2}`, :math:`\dfrac{2}{4}` et :math:`\dfrac{3}{4}`.
	#.	La méthode ``__eq__`` compare deux nombres et renvoie un booléen. On utilise l'opérateur ``==`` pour effectuer cette comparaison. Écrire cette méthode dans la classe ``Fraction`` puis la tester avec les objets ``a``, ``b`` et ``c``.
	#.	Écrire les méthodes ``__lt__`` et ``__gt__`` associées aux opérateurs ``<`` et ``>`` qui renvoient un booléen. Tester vos méthodes avec les objets créés.

	#.  L'opérateur ``+`` est associé à la méthode ``__add__``. Écrire cette méthode dans la classe ``Fraction`` qui additionne deux objets ``Fraction``. La méthode prend en paramètre un objet ``Fraction`` et renvoie un objet ``Fraction`` dont la valeur est la somme des 2 fractions. Tester votre méthode avec les objets ``a``, ``b`` et ``c``.
	#.	L'opérateur ``*`` est associé à la méthode ``__mul__``. Écrire cette méthode dans la classe ``Fraction`` qui multiplie deux objets ``Fraction``. La méthode prend en paramètre un objet ``Fraction`` et renvoie un objet ``Fraction`` dont la valeur est le produit des 2 fractions. Tester votre méthode avec les objets ``a``, ``b`` et ``c``.

.. exercice::

	On va créer une classe ``Date`` pour représenter une date avec trois attributs ``jour``, ``mois`` et ``annee``.

	#.  Écrire son constructeur avec les paramètres j, m et a.
	#.  Une variable de classe est utilisée dans la classe par différentes méthodes. Cette variable est créée dans la classe mais en dehors du constructeur. L'accès à cette variable se fait avec la syntaxe ``nom de la classe.variable``. Créer une variable de classe ``mois`` de type liste contenant les douze mois de l'année sous forme de chaine de caractères.
	#.  Ajouter une méthode ``__str__`` qui renvoie une chaine de caractères de la forme "11 novembre 1918". Tester l'affichage avec la commande ``print``.

	#.  Ajouter une méthode ``__lt__`` qui permet de déterminer si une date ``d1`` est antérieure à une date ``d2`` en écrivant ``d1 < d2``. La tester.

	#. 	a. 	Modifier le constructeur avec des valeurs par défaut initialisées au 1 janvier 2000.

		b. Créer un objet ``Date``, nommé ``ddn``, sans paramètres. Vérifier que les attributs de ``ddn`` ont pour valeurs la date du 1 janvier 2000.

		c. Modifier les attributs ``ddn`` avec les dates de votre anniversaire.
	
	#. 	En Python, il est possible de modifier la valeur des attributs d'un objet. Cela peut poser des problèmes surtout lorsqu'on a des attributs dont les valeurs ne doivent pas être accessibles. On peut interdire l'accès aux attributs en les cachant. Il suffit d'ajouter un double souligné devant le nom de l'attribut : ``self.__attribut``.
	
		Modifier dans le constructeur les attributs ``jour``, ``mois`` et ``annee`` pour qu'ils soient cachés. Vérifier qu'il n'est plus possible de modifier les valeurs d'une date une fois créée.
		
		.. warning::
			
			Penser à modifier les méthodes qui utilisent ces attributs cachés !
			
	#.	Pour accéder aux attributs cachés et les modifier, on peut créer des méthodes particulières appelées **accesseurs** et **mutateurs**. On définit pour l'attribut caché ``jour``, l'accesseur ``get_jour`` et le mutateur ``set_jour`` de la manière suivante:

		.. image:: ../img/poo1.jpg
			:align: center
			:width: 560px
	
		a.	Ajouter ces deux méthodes dans la classe ``Date`` et vérifier que vous pouvez afficher et modifier le jour d'une date
		b. 	Ajouter les accesseurs et les mutateurs pour le mois et l'année.
		c. 	Vérifier, après avoir créé une date ``ddn`` sans paramètres, que vous pouvez la modifier par votre date de naissance.
