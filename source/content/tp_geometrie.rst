TP : Géométrie
===============

En géométrie, il est possible de repésenter des figures géométriques en utilisant un repère. Chaque point est repéré par un couple de nombres appelés ``abscisse`` et ``ordonnée``. Avec 2 points, on peut créer un segement et avec plusieurs points, on crée des figure géométriques.

.. figure:: ../img/point_segment.svg
   :align: center
   :width: 400

L'objectif du TP est la programmation en Python de points pour créer des figures géométriques simples.

La classe Point
-------------------

#.	La classe ``Point`` permet de créer des objets ayant 2 attributs : ``abscisse`` et ``ordonnée``. On donne le code python de la classe ``Point``:

	.. code-block:: python

		class Point:

			def __init__(self,x,y):
				self.x = x # abscisse du point
				self.y = y # ordonnée du point
				print("Point construit!")

	a.  Recopier le code de cette classe dans un fichier python nommé ``geometrie.py``.
	b.  En console, l'instruction ``A = Point(1,2)`` construit un objet nommé ``A`` qui a 2 attributs ``x`` de valeur ``1`` et ``y`` de valeur ``2``. COnstruire le point ``A`` puis le point ``B(3,4)``.
	c.  Modifier les coordonnées du point B en les remplaçant par les coordonnées ``(4,-2)``.
	d.  Écrire une fonction ``affiche`` qui prend en paramètre un point et affiche les coordonnées de ce point.

#.  On calcule la distance [1]_ entre 2 points A et B avec la formule mathématique :math:`\sqrt{(x_A-x_B)^{2}+(y_A-y_B)^{2}}` [2]_.

    Écrire en python la fonction ``distance`` qui prend en paramètre 2 points et renvoie la distance entre ces 2 points, arrondie à 2 chiffres après la virgule.

    .. note::

        #.  La racine carrée se calcule avec la fonction ``sqrt`` du module ``math`` qu'il faut importer.
        #.  La fonction ``round(v,n)`` arrondit la valeur ``v`` passée en argument avec ``n`` chiffres après la virgule. La valeur renvoyée est donc de type ``float``.

#.  En l'état, il n'est pas possible d'afficher le nom de l'objet. Un moyen est donc de créer un attribut qui sera associé au nom du point.

    a. Ajouter l'attribut ``nom`` à la classe ``Point``.
    b. Après avoir apporté les modifications nécessaires, recréer les points ``A`` et ``B`` puis afficher ces points avec leurs noms!

La classe Segment
----------------------

Un segment est la partie de droite comprise entre 2 points. 

La classe ``Segment`` représente un segment qui a pour extrémités 2 points, c'est à dire 2 objets de la classe ``Point``. Cela signifie que chaque objet ``Segment`` a deux attributs ``ext1`` et ``ext2`` correspondant à 2 objets de la classe Point.

#.  Écrire en python la classe ``Segment`` et son constructeur qui prend en argument 2 objets de la classe ``Point``. Par exemple, avec les points ``A`` et ``B`` on construit le segment ``AB``.

    >>> AB = Segment(A,B)

#.  On souhaite avoir une méthode qui renvoie la longueur d'un segment.

    a.  En reprenant le code de la fonction distance, ajouter à la classe ``Segment`` la méthode ``longueur`` qui a pour unique paramètre ``self``. Cette méthode renvoie la longueur du segment.
    b.  Après avoir créer le segment ``AB``, calculer sa longueur avec la methode dédiée.
   
La classe Cercle 
---------------------

La classe ``Cercle`` crée des objets dont les attributs sont un objet ``Point`` et un nombre pour le rayon. Lorsqu'on construit un objet de type ``Cercle``, le centre et le rayon sont passés en arguments.

#.  Écrire le constructeur de la classe définissant les attributs ``centre`` et ``rayon``.
#.  Écrire la méthode ``aire`` qui calcule l'aire du cercle avec la formule :math:`\pi \times rayon^{2}`.

La classe Triangle
---------------------------

- La classe ``Triangle`` crée des objets dont les attributs sont trois côtés définis comme objets ``Segment`` Lorsqu'un objet de type ``Triangle`` est construit, les trois points sommets sont passés en arguments.

#.  Écrire le constructeur de la classe définissant les attributs ``cote_1``, ``cote_2`` et ``cote_3``. On rappelle que ce sont trois points qui sont passés en argument lors de la construction d'un objet.
#.	La formule de Héron permet de calculer l'aire d'un triangle avec les longueurs des 3 côtés du triangle. On en donne la formule: :math:`\sqrt{p(p-a)(p-b)(p-c)}` où :math:`a`, :math:`b`, :math:`c` sont les longueurs des côtés du triangle et :math:`p` le demi-périmètre du triangle.

	Écrire la méthode ``aire`` qui renvoie l'aire d'un triangle calculée avec la méthode de Héron.



.. [1] le calcul d'une distance entre 2 points d'un repère n'est possible que si le repère utilisé est **orthonormé** mais cela dépasse le cadre de ce cours et donc on admet que de telles conditions sont réunies.
.. [2] l'ordre des abscisses et des ordonnées des points A et B n'a pas d'importance car :math:`(x_A-x_B)^{2}=(x_B-x_A)^{2}`.